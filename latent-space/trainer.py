import model


class StyleTransferTrainer:
    def __init__(self, batch_size, lr, beta1, beta2, gpu, root,
                 n_epoch, continue_from, checkpoint_every, random_seed, embeddings):
        self.root = root
        self.batch_size = batch_size
        self.n_epoch = n_epoch
        self.checkpoint_every = checkpoint_every
        self.gpu = gpu
        self.random_seed = random_seed
        self.lr = lr
        self.beta1 = beta1
        self.beta2 = beta2
        self.continue_from = continue_from
        self.embeddings = embeddings

        # TODO: define model
        # self.encoder = model.EncoderRNN()
        # self.decoder = model.DecoderRNN()

    def train_iter(self):
        pass

    def train(self):
        pass

