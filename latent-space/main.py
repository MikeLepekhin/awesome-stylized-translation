import exman
from trainer import StyleTransferTrainer

parser = exman.ExParser(root=exman.simpleroot("../experiments"), git=True)
parser.add_argument('--batch_size', type=int, default=64)
parser.add_argument('--lr', type=float, default=2e-4)
parser.add_argument('--beta1', type=float, default=0.5)
parser.add_argument('--beta2', type=float, default=0.999)
parser.add_argument('--n_epoch', type=int, default=300)
parser.add_argument('--continue_from', type=str, default=None)
parser.add_argument('--checkpoint_every', type=int, default=25)
parser.add_argument('--gpu', type=int, default=None)
parser.add_argument('--seed', type=int, default=42)
parser.add_argument('--embeddings', type=str, default='bert')

if __name__ == '__main__':
    args = parser.parse_args()
    with args.safe_experiment:
        style_transfer_trainer = \
            StyleTransferTrainer(batch_size=args.batch_size,
                                 lr=args.lr,
                                 beta1=args.beta1,
                                 beta2=args.beta2,
                                 gpu=args.gpu,
                                 root=args.root,
                                 n_epoch=args.n_epoch,
                                 continue_from=args.continue_from,
                                 checkpoint_every=args.checkpoint_every,
                                 random_seed=args.seed,
                                 embeddings=args.embeddings)

        style_transfer_trainer.train()
